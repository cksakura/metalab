/**
 * 
 */
/**
 * 
 * https://www.nature.com/articles/s41587-020-0603-3
 * 
 * A unified catalog of 204,938 reference genomes from the human gut microbiome
 * 
 * @author Kai Cheng
 *
 */
package bmi.med.uOttawa.metalab.core.MGYG.db;