package bmi.med.uOttawa.metalab.dbSearch.msFragger;

public class InteractPSM {
	
	private int start_scan;
	private int assumed_charge;
	private double precursor_neutral_mass;
	private double retention_time_sec;
	private String peptide;
	private double massdiff;
	private int num_missed_cleavages;
	private String protein;
	
	private double hyperscore;
	private double nextscore;
	private double expect;
	
	private double qvalue;
	private double probability;
	
	
}
