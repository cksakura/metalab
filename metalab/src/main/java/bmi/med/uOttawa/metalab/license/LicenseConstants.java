package bmi.med.uOttawa.metalab.license;

public class LicenseConstants {

	public static final String NAME_STRING = "name";

	public static final String AFFILIATION_STRING = "affiliation";

	public static final String COUNTRY_STRING = "country";

	public static final String EMAIL_STRING = "email";

	public static final String IP_STRING = "ip";

	public static final String MAC_STRING = "mac";

	public static final String BASEBOARD_STRING = "baseboard";

	public static final String DISKDRIVE_STRING = "diskdrive";

	public static final String BIOS_STRING = "bios";

	public static final String CPU_STRING = "cpu";

	public static final String ISSUEDATE_STRING = "issuedTime";

	public static final String BEGINDATE_STRING = "notBefore";

	public static final String ENDDATE_STRING = "notAfter";

	public static final String LICENSETYPE_STRING = "licenseType";

	public static final String FREE_STRING = "free";

	public static final String PAID_STRING = "paid";

	public static final String Trail_STRING = "trial";

	public static final String[] DBTITLE_STRINGS = new String[] { NAME_STRING, AFFILIATION_STRING, COUNTRY_STRING,
			EMAIL_STRING, IP_STRING, MAC_STRING, BASEBOARD_STRING, DISKDRIVE_STRING, BIOS_STRING, CPU_STRING,
			ISSUEDATE_STRING, BEGINDATE_STRING, ENDDATE_STRING, LICENSETYPE_STRING };

	public static final String INHERENTPATH_STRING = "params.properties";
	
	public static final String KEYALIAS_STRING = "public.alias";
	
	public static final String STOREPWD_STRING = "key.store.pwd";
	
	public static final String PRIPATH_STRING = "priPath";

	public static final String SUBJECT_STRING = "subject";

	public static final String PUBPATH_STRING = "/publicCerts.store";

}
