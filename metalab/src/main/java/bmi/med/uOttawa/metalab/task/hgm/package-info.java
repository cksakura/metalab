/**
 * {@link http://ftp.ebi.ac.uk/pub/databases/metagenomics/mgnify_genomes/human-gut/v2.0/}
 */
/**
 * 
 * {@link http://ftp.ebi.ac.uk/pub/databases/metagenomics/mgnify_genomes/human-gut/v2.0/}
 * <p>
 * {@link https://www.nature.com/articles/s41587-020-0603-3}
 * @author Kai Cheng
 * 
 * 
 */
package bmi.med.uOttawa.metalab.task.hgm;