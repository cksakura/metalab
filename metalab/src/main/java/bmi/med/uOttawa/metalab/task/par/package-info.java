/**
 * From MetaLab version 2.0.0, the parameters were stored in two parameter files instead of a single file.
 * 
 * 
 */
/**
 * @author Kai Cheng
 *
 */
package bmi.med.uOttawa.metalab.task.par;