/**
 * 
 */
package bmi.med.uOttawa.metalab.core.taxonomy;

/**
 * @author Kai Cheng
 *
 */
public final class TaxonomyParameter {
	
	public static final int homo_sapiens_id = 1;
	public static final int mus_id = 2;
}
