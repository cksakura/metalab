package bmi.med.uOttawa.metalab.glycan.deNovo.microbiota.IO;

public class MetaGlycanConstants {
	
	public static String standardMode = "Standard";
	
	public static String discoverMode = "Discovery";
	
	public static String onlyGlycanMode = "Only glycan";

}
